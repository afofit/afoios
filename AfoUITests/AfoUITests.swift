//
//  AfoUITests.swift
//  AfoUITests
//
//  Created by mohan.k on 29/01/21.
//

import XCTest

class AfoUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
  
  private func openFromMessages(_ urlString: String) {
    
    
    let messages = XCUIApplication(bundleIdentifier: "com.apple.MobileSMS")
    messages.launch()
    XCTAssert(messages.wait(for: .runningForeground, timeout: 5))
    
    // Dismiss "What's New" if needed
    let continueButton = messages.buttons["Continue"]
    if continueButton.exists {
      continueButton.tap()
    }
    // Dismiss iOS 13's "New Messages" if needed
    let cancelButton = messages.navigationBars.buttons["Cancel"]
    if cancelButton.exists {
      cancelButton.tap()
    }
    
    // Open the first available chat
    let chat = messages.cells.firstMatch
    XCTAssertTrue(chat.waitForExistence(timeout: 5))
    chat.tap()
    // Tap the text field
    messages.textFields["iMessage"].tap()
    
    // Dismiss Keyboard tutorial if needed
    let keyboardTutorialButton = messages.buttons["Continue"]
    if keyboardTutorialButton.exists {
      keyboardTutorialButton.tap()
    }
    messages.typeText("link")
    messages.buttons["sendButton"].tap()
    
    messages.typeText("https://www.google.com")
    messages.buttons["sendButton"].tap()
    
    let bubble = messages.links.firstMatch
    bubble.waitForExistence(timeout: 5)
//    XCTAssertTrue()
    sleep(3)
    bubble.tap()
    
  }
  
  func testUniversalLinks() {
//    let app = XCUIApplication()
//    app.launch()
    // Launch Messages and univesal link back to our app

    
    openFromMessages("https://swiftrocks.com/profile")
    
    
  }
  
    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
