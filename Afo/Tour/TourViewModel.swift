//
//  TourViewModel.swift
//  Afo
//
//  Created by mohan.k on 17/03/21.
//

import Foundation
import UIKit

struct  TourViewModel {
  var currentPage: Int = 0
  let images: [UIImage]?
  init?() {
    guard let image1 = UIImage(named: "tourPage1"),
          let image2 = UIImage(named: "tourPage2"),
          let image3 = UIImage(named: "tourPage3") else {
      return nil
    }
    self.images = [image1,image2,image3]
  }
}
