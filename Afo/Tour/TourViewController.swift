//
//  TourViewController.swift
//  Afo
//
//  Created by mohan.k on 17/03/21.
//

import UIKit

class TourViewController: UIViewController, UIGestureRecognizerDelegate {
  
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var collectionView: UICollectionView!
  
  @IBOutlet weak var startButton: UIButton!
  var homeViewController: ViewController?

  var tourViewModel = TourViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.collectionView.delegate = self
    self.collectionView.dataSource = self
    
    self.pageControl.numberOfPages = tourViewModel?.images?.count ?? 0
    self.pageControl.currentPage = 0
    self.collectionView.scrollToItem(at: IndexPath(row: self.pageControl.currentPage, section: 0), at: .centeredHorizontally, animated: true)
    startButton.roundEdges()
    
    homeViewController = storyboard?.instantiateViewController(withIdentifier: "homeViewController") as? ViewController
    homeViewController?.loadViewIfNeeded()
    homeViewController?.perfomFirstLaunchActions()
  }
  @IBAction func startButtonTapped(_ sender: Any) {
    var vcToShow: UINavigationController?
    vcToShow = storyboard?.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController
    guard let vc = vcToShow else {
      return
    }
    if let homevc = homeViewController{
      vc.viewControllers = [homevc]
    }
    DispatchQueue.main.async {
      UIApplication.shared.windows.first?.rootViewController = vcToShow
      UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
  }
  
}

extension TourViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return tourViewModel?.images?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "tourCell", for: indexPath) as!
      TourCollectionViewCell
    cell.tourImageView.setImage(tourViewModel?.images?[indexPath.row] ?? UIImage())
    return cell
  }
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: view.frame.size.width, height: view.frame.size.height)
  }
  
}


