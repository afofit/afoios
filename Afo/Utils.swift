//
//  Utils.swift
//  Afo
//
//  Created by mohan.k on 10/03/21.
//

import Foundation
import UIKit
import HealthKit

class Utils {
  
  static func saveFIRDynamicLinkIfAvailable(dynamicLink: String?){
    UserDefaults.standard.setValue(dynamicLink, forKey: "dynamicLink")
  }
  
  static func setPrimaryHealthSource(primaryHealthSource: String){
    UserDefaults.standard.setValue(primaryHealthSource, forKey: "primaryHealthSource")
  }
  
  static func showAlert(_ alertController: UIAlertController) {
    DispatchQueue.main.async {
      guard let rootVC = Utils.getRootVC() else { return }
      rootVC.present(alertController, animated: true, completion: nil)
    }
  }
  
  static func getRootVC() -> UIViewController? {
    return UIApplication.shared.windows.first?.rootViewController
  }
  
  static func getAnchor(forKey key: String) -> HKQueryAnchor? {
    guard let anchorData = UserDefaults.standard.data(forKey: key) else {return nil}
    do {
      let anchor = try NSKeyedUnarchiver.unarchivedObject(ofClass: HKQueryAnchor.self, from: anchorData)
      return anchor
      
    } catch {
      print("Failed to unarchive")
      return nil
    }
  }
  
  static func updateAnchor(forKey key: String, anchor: HKQueryAnchor) {
    do {
      let anchorData = try NSKeyedArchiver.archivedData(withRootObject: anchor, requiringSecureCoding: true)
      UserDefaults.standard.setValue(anchorData, forKey: key)
    } catch {
      print("Failed to archive")
    }
  }
  
  static func hasShownTour() -> Bool {
    return UserDefaults.standard.bool(forKey: "hasTourCompleted")
  }
  
  static func setTourAsSeen(){
    UserDefaults.standard.setValue(true, forKey: "hasTourCompleted")
  }
  //MARK: FixMe
  static func getLastActivityUpdatedTime()-> Date {
    if let lut = FirestoreManager.shared.userDataDict?["LastActivityUpdated"] as? Int, lut != 0 {
      return Date.getDateFromActivityStartEpochDate(fromTimeIntervalSince1970: lut)
    }
    return Date().previous(.sunday).previous(.sunday).getDayStartDate()
  }
  
  static func showHealthAlert(title: String, withMessage: String,
                              okCompletionHandler : ((UIAlertAction) -> Void)? = nil,
                              cancelCompletionHandler: ((UIAlertAction) -> Void)? = nil ) {
      let alertViewController = UIAlertController(title: title, message: withMessage, preferredStyle: .alert)
      let okAction = UIAlertAction(title: "Ok", style: .default, handler: okCompletionHandler)
      let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: cancelCompletionHandler)
      alertViewController.addAction(okAction)
      alertViewController.addAction(cancelAction)
      Utils.showAlert(alertViewController)
  }
}


extension Date {
  
  func distanceTo(to otherDate: Date) -> TimeInterval {
    if #available(iOS 13.0, *) {
      return self.distance(to: otherDate)
    } else {
      // Fallback on earlier versions
      return  TimeInterval(otherDate.timeIntervalSince1970 - self.timeIntervalSince1970)
    }
  }
  
  static func getFormattedString(fromDate: Date) -> String {
    let df = DateFormatter()
    df.timeZone = .autoupdatingCurrent
    df.dateFormat = "ddMMyyyyHHmmss"
    return df.string(from: fromDate)
  }
  
  static func decryptDate(fromDateString dateString: String?, usingFormat format: String) -> Date? {
    guard let dateString = dateString else {
      return nil
    }
    let df = DateFormatter()
    df.dateFormat = format
    df.timeZone = .autoupdatingCurrent
    return df.date(from: dateString)
  }
  
  func yearsTillNow() -> Int {
    let yearComponent = Calendar.current.dateComponents([.year], from: self, to: Date())
    return yearComponent.year ?? 0
  }
  
  func getEpochValue() -> Int {
    var timeIntervalSince1970 = self.timeIntervalSince1970
    timeIntervalSince1970 = timeIntervalSince1970 * 1000
    return Int(timeIntervalSince1970)
  }
  
  func rounded() -> Date{
    let intValue = Int(self.timeIntervalSince1970)
    return Date(timeIntervalSince1970: TimeInterval(intValue))
  }
  
  func roundedToClosestMinute() -> Date {
    let calender = Calendar.current
    let secondsInDate = calender.component(.second, from: self)
    if secondsInDate > 30 {
      var dateComponents = DateComponents()
      dateComponents.second = 60 - secondsInDate
      return calender.date(byAdding: dateComponents, to: self) ?? self
    } else {
      return calender.date(bySetting: .second, value: 0, of: self) ?? self
    }
  }
  
  func getDayStartDate() -> Date {
    var calender = Calendar.autoupdatingCurrent
    calender.timeZone = .autoupdatingCurrent
    return calender.startOfDay(for: self)
  }
  
  func getWeekStartDate() -> Date {
    return self.previous(.sunday)
  }
  
  func getCalenderID() -> String{
    let df = DateFormatter()
    df.timeZone = .autoupdatingCurrent
    df.dateFormat = "dd-MM-YYYY"
    let dateString = df.string(from: self)
    return dateString + "_" + userID!
  }
  
  func getDateString(inFormat format: String) -> String {
    let df = DateFormatter()
    df.dateFormat = format
    df.timeZone = .autoupdatingCurrent
    let dateString = df.string(from: self)
    return dateString
  }
  
  static func getDateFromActivityStartEpochDate(fromTimeIntervalSince1970 date:Int) -> Date{
    let date = Date(timeIntervalSince1970: Double(date/1000))
    return date.getDayStartDate()
  }
  
  static func getDateFromEpochValue(fromTimeIntervalSince1970 date:Int) -> Date{
    let date = Date(timeIntervalSince1970: Double(date/1000))
    return date
  }
  
  func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
    return get(.next,
               weekday,
               considerToday: considerToday)
  }
  
  func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
    return get(.previous,
               weekday,
               considerToday: considerToday)
  }
  
  func get(_ direction: SearchDirection,
           _ weekDay: Weekday,
           considerToday consider: Bool = false) -> Date {
    
    let dayName = weekDay.rawValue
    
    let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
    
    assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
    
    let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1
    
    let calendar = Calendar(identifier: .gregorian)
    
    if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
      return self
    }
    
    var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
    nextDateComponent.weekday = searchWeekdayIndex
    
    let date = calendar.nextDate(after: self,
                                 matching: nextDateComponent,
                                 matchingPolicy: .nextTime,
                                 direction: direction.calendarSearchDirection)
    
    return date!
  }
  
}

// MARK: Helper methods
extension Date {
  func getWeekDaysInEnglish() -> [String] {
    var calendar = Calendar(identifier: .gregorian)
    calendar.locale = Locale(identifier: "en_US_POSIX")
    return calendar.weekdaySymbols
  }
  
  enum Weekday: String {
    case monday, tuesday, wednesday, thursday, friday, saturday, sunday
  }
  
  enum SearchDirection {
    case next
    case previous
    
    var calendarSearchDirection: Calendar.SearchDirection {
      switch self {
        case .next:
          return .forward
        case .previous:
          return .backward
      }
    }
  }
}

func openUrl(urlString: String) {
  guard let url = URL(string: urlString) else {
    return
  }
  
  if UIApplication.shared.canOpenURL(url) {
    UIApplication.shared.open(url, options: [:])
  }
}


extension URL {
  var parameters: [String: String?]?
  {
    if  let components = URLComponents(url: self, resolvingAgainstBaseURL: false),
        let queryItems = components.queryItems
    {
      var parameters = [String: String?]()
      for item in queryItems {
        parameters[item.name] = item.value
      }
      return parameters
    } else {
      return nil
    }
  }
}

extension UIView{
  func roundEdges() {
    self.layer.cornerRadius = 10
  }
}

extension UIViewController {
  func showLoader() {
    self.hideLoader()
    if let vc = self.presentedViewController, vc is UIAlertController, vc.view.tag == 1 {
      return
    }
    let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    loadingIndicator.style = .gray
    loadingIndicator.startAnimating();
    
    alert.view.addSubview(loadingIndicator)
    alert.view.tag = 1
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: nil)
    }
  }
  
  func hideLoader() {
    if let vc = self.presentedViewController, vc is UIAlertController, vc.view.tag == 1 {
      DispatchQueue.main.async {
        self.dismiss(animated: false, completion: nil)
      }
    }
  }
  
  func openActivityViewController(string: String?, url: URL?, image: UIImage?) {
    var activityItemsToShare: [Any] = [string ,UIImage(named: "AppLogo")!]
    let activityViewController =
      UIActivityViewController(activityItems: activityItemsToShare,
                               applicationActivities: nil)
    activityViewController.excludedActivityTypes = [.assignToContact, .markupAsPDF, .saveToCameraRoll,.addToReadingList,.openInIBooks]
    present(activityViewController, animated: true)
  }
  
}


extension UserDefaults {
  @objc dynamic var dynamicLink: String? {
    return string(forKey: "dynamicLink")
  }
  
  @objc dynamic var primaryHealthSource: String? {
    return string(forKey: "primaryHealthSource")
  }
}
