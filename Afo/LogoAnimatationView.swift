//
//  LogoAnimatationView.swift
//  Afo
//
//  Created by mohan.k on 01/02/21.
//

import UIKit
import SwiftyGif

class LogoAnimationView: UIView {
  
  var logoGifImageView: UIImageView = UIImageView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    backgroundColor = .black
    do {
      let gif = try UIImage(gifName: "afoSplashBlack.gif")
      logoGifImageView = UIImageView(gifImage: gif, loopCount: 1)
    } catch {
      
    }
    addSubview(logoGifImageView)
    logoGifImageView.contentMode = .scaleAspectFill
    logoGifImageView.translatesAutoresizingMaskIntoConstraints = false
    logoGifImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    logoGifImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    logoGifImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    logoGifImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    logoGifImageView.animationRepeatCount = 1
  }
  
}
