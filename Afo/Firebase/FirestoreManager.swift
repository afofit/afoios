//
//  FirestoreManager.swift
//  Fitty
//
//  Created by mohan.k on 28/01/21.
//

import Foundation
import Firebase
import FirebaseFirestore

var userID: String? = UserDefaults.standard.string(forKey:"user_id")

class FirestoreManager {
  
  static var shared = FirestoreManager()
  let db: Firestore
  let userRef: CollectionReference
  let calendarRef: CollectionReference
  let activitiesRef: CollectionReference
  
  var dispatchGroup = DispatchGroup()
  
  var userDoc: DocumentReference!
  var userDataDict: [String: Any]?
  var userListener: ListenerRegistration?
  
  private init () {
    let settings = FirestoreSettings()
    settings.isPersistenceEnabled = true
    settings.cacheSizeBytes = FirestoreCacheSizeUnlimited
    self.db = Firestore.firestore()
    self.db.settings = settings
    self.userRef = db.collection("users");
    self.calendarRef = db.collection("calendar")
    self.activitiesRef = db.collection("activities")
  }
  
  public func setupUserData(completionHandler: @escaping () -> Void = {}) {
    if let userId = userID, userId != "" {
      let udg = DispatchGroup()
      self.userDoc = self.userRef.document(userId)
      
      udg.enter()

      self.userDoc.getDocument { snapshot, error in
        defer { udg.leave() }
        guard let snapshot = snapshot else {
          return
        }
        self.userDataDict = snapshot.data()
      }
      
      udg.notify(queue: .main, execute: completionHandler)



      // Add observer
      self.userListener?.remove()
      self.userDoc = userRef.document(userId)
      self.userListener = self.userDoc?.addSnapshotListener {snapshot, error in

        guard let snapshot = snapshot else {return}
        self.userDataDict = snapshot.data()
      }
      if self.userDataDict == nil {self.userDataDict = [:]}
    }
  }
  
  public func updateUserData (forActivities activities: [[String: Any]] ){
    let startOfCurrentWeek = Date().get(.previous, .sunday, considerToday: true).getDayStartDate()
    
    if let userID = userID {
      self.calendarRef.whereField("UserID", isEqualTo: userID).getDocuments(source: .cache){ snapshot, error in
        if error != nil {
          return
        }
        guard let calendars = snapshot?.documents, calendars.count > 0 else {return}
        var weeklyFitScore = 0
        var totalNumberOfActivities = 0
        var calories = 0
        var fitscore = 0
        var steps = 0
        var duration = 0
        var userData:[String:Any] = [:]
        for calendar in calendars {
          totalNumberOfActivities += 1
          calories += (calendar["DailyCalories"] as? Int) ?? 0
          fitscore += (calendar["DailyFitScore"] as? Int) ?? 0
          duration += (calendar["DailyDuration"] as? Int) ?? 0
          steps += (calendar["DailySteps"] as? Int) ?? 0
          if let startDateEpoch = calendar["RawDate"] as? Int {
            let startDate = Date.getDateFromEpochValue(fromTimeIntervalSince1970: startDateEpoch)
            if startDate.compare(startOfCurrentWeek) == .orderedDescending {
              let activityfitScore = (calendar["DailyFitScore"] as? Int) ?? 0
              weeklyFitScore += activityfitScore
            }
          }
        }
        userData["Count"] = totalNumberOfActivities
        userData["WeeklyFitScore"] = weeklyFitScore
        userData["Calories"] = calories
        userData["FitScore"] = fitscore
        userData["Steps"] = steps
        userData["Duration"] = duration
        userData["LastActivityUpdated"] = Date().getEpochValue()
        userData["UserID"] = userID
        userData["PrimarySourceName"] = "AppleHealth"
        userData["PrimarySourceID"] = "3"
        self.userDoc?.setData(userData, merge: true, completion: {error in if (error == nil ) {
          self.userDataDict = userData
          print("Successfully updated user data for user " + userID + " in firestore")
        }})
      }
    }
  }
  
  
//
//
//
//  var userData = self.userDataDict ?? [:]
//  var weeklyFitScore = (userData["WeeklyFitScore"] as? Int) ?? 0
//  var totalNumberOfActivities = (userData["Count"] as? Int) ?? 0
//  var calories = (userData["Calories"] as? Int) ?? 0
//  var fitscore = (userData["WeeklyFitScore"] as? Int) ?? 0
//  var steps = (userData["FitScore"] as? Int) ?? 0
//  var duration = (userData["Duration"] as? Int) ?? 0
//  for activity in activities {
//  totalNumberOfActivities += 1
//  calories += (activity["CaloriesBurned"] as? Int) ?? 0
//  fitscore += (activity["ActivityScore"] as? Int) ?? 0
//  duration += (activity["Duration"] as? Int) ?? 0
//  steps += (activity["StepCount"] as? Int) ?? 0
//
//  if let startDateEpoch = activity["StartDate"] as? Int {
//  let startDate = Date(timeIntervalSince1970: TimeInterval(startDateEpoch))
//  if startDate.compare(startOfCurrentWeek) == .orderedDescending {
//  let activityfitScore = (activity["ActivityScore"] as? Int) ?? 0
//  weeklyFitScore += activityfitScore
//  }
//  }
//  }
//  userData["Count"] = totalNumberOfActivities
//  userData["WeeklyFitScore"] = weeklyFitScore
//  userData["Calories"] = calories
//  userData["FitScore"] = fitscore
//  userData["Steps"] = steps
//  userData["Duration"] = duration
//  userData["LastActivityUpdated"] = Date().getEpochValue()
//  userData["UserID"] = userID
//  userDoc?.setData(userData, merge: true, completion: {error in if (error == nil ) {
//  self.userDataDict = userData
//  UserDefaults.standard.setValue(userData["LastActivityUpdated"] as! Int, forKey: "LastActivityUpdated")
//  print("Successfully updated user data for user " + userID + " in firestore")
//  }})
  
  //  public func updateUserData() {
  //    var userData: [String : Any] = [
  //      "UserID": userID,
  //      "PrimarySourceID": "3",
  //      "PrimarySourceName": "AppleHealth",
  //      "LastActivityUpdated": Date().getEpochValue(),
  //      "AccessToken": "",
  //      "Count": 0,
  //      "WeeklyFitScore": 0
  //    ]
  //    let startOfCurrentWeek = Date().get(.previous, .sunday, considerToday: true)
  //
  //    if let userID = userID {
  //      let userDoc = userRef.document(userID)
  //      db.collection("activities").whereField("UserID", isEqualTo: userID)
  //        .getDocuments() { (querySnapshot, err) in
  //          if let err = err {
  //            print("Error getting documents: \(err)")
  //          } else if querySnapshot != nil{
  //            var weeklyFitScore = 0
  //            var totalNumberOfActivities = 0
  //            var calories = 0
  //            var fitscore = 0
  //            var steps = 0
  //            var duration = 0
  //            for document in querySnapshot!.documents {
  //              totalNumberOfActivities += 1
  //              calories += (document.get("CaloriesBurned") as? Int) ?? 0
  //              fitscore += (document.get("ActivityScore") as? Int) ?? 0
  //              duration += (document.get("Duration") as? Int) ?? 0
  //              steps += (document.get("StepCount") as? Int) ?? 0
  //
  //              if let startDateEpoch = document.get("StartDate") as? Int {
  //                let startDate = Date(timeIntervalSince1970: TimeInterval(startDateEpoch))
  //                if startDate.compare(startOfCurrentWeek) == .orderedDescending {
  //                  let fitScore = (document.get("ActivityScore") as? Int) ?? 0
  //                  weeklyFitScore += fitScore
  //                }
  //              }
  //
  //            }
  //            userData["Count"] = totalNumberOfActivities
  //            userData["WeeklyFitScore"] = weeklyFitScore
  //            userData["Calories"] = calories
  //            userData["FitScore"] = fitscore
  //            userData["Steps"] = steps
  //            userData["Duration"] = duration
  //            userDoc.setData(userData, merge: true, completion: {error in if (error == nil ) {  print("Successfully updated user data for user " + userID + " in firestore")}})
  //          }
  //        }
  //      //      userData["Count"] = totalNumberOfActivities
  //      //      userData["WeeklyFitScore"] = weeklyFitScore
  //      //
  //      //      userDoc.setData(userData, merge: true, completion: {error in if (error == nil ) {  print("Successfully updated user data for user " + userID + " in firestore")}})
  //    } else {
  //      return
  //    }
  //  }
  
  public func updateUserActivity(activity: [String: Any]?) {
    guard let activity = activity,
          let activityID = activity["ActivityID"] as? String,
          let activityName = activity["ActivityName"] as? String,
          let startDateEpoch = activity["StartDate"] as? Int else {
      print("Passed invalid activity to update")
      return
    }
    
    let activityDate: Date = Date.getDateFromActivityStartEpochDate(fromTimeIntervalSince1970: startDateEpoch)
    let activityDoc = activitiesRef.document(activityID)
    
    //    var lastUpdatedTime = Date().previous(.sunday).previous(.sunday).getDayStartDate()
    //    if let lut = Utils.getLastActivityUpdatedTime() {
    //      lastUpdatedTime = Date.getDateFromEpochValue(fromTimeIntervalSince1970: lut)
    //      if activityName == "Daily Steps" {
    //        lastUpdatedTime = lastUpdatedTime.getDayStartDate()
    //      }
    //    }
    //
    //    if activityDate.compare(lastUpdatedTime) == .orderedAscending{
    //      return
    //    }
    print("Updating activity with name - \(activityName)  data - \(activity)")
    activityDoc.setData(activity,
                        merge: true,
                        completion: { error in
                            if (error == nil ) {
                              print("Successfully updated activity data for activity " + activityName + " in firestore data - \(activity)")
                              self.updateCalender(forActivity: activity)
                            }
                            else {
                              print("Failed to update user activity with error - \(error!.localizedDescription)")
                            }
                        })
  }
  
  public func updateActivities(activities: [[String: Any]]){
    var activitiesReadyForUpdate: [[String: Any]] = []
    for activity in activities {
      guard let activityName = activity["ActivityName"] as? String,
            let startDateEpoch = activity["StartDate"] as? Int else {
        print("Passed invalid activity to update")
        continue
      }
      
      let activityDate: Date = Date.getDateFromActivityStartEpochDate(fromTimeIntervalSince1970: startDateEpoch)
      
      //      var lastUpdatedTime = Date().previous(.sunday).previous(.sunday).getDayStartDate()
      //      if let lut = Utils.getLastActivityUpdatedTime() as? Int {
      //        lastUpdatedTime = Date.getDateFromEpochValue(fromTimeIntervalSince1970: lut)
      //        if activityName == "Daily Steps" {
      //          lastUpdatedTime = lastUpdatedTime.getDayStartDate()
      //        }
      //      }
      //
      //      if activityDate.compare(lastUpdatedTime) == .orderedAscending{
      //        continue
      //      }
      print("Updating activity with name - \(activityName)  data - \(activity)")
      activitiesReadyForUpdate.append(activity)
    }
    if activitiesReadyForUpdate.count == 0{
      print("No New activities to update")
      return
    }
    let batch = self.db.batch()
    for eachActivity in activitiesReadyForUpdate {
      let activityDoc = activitiesRef.document(eachActivity["ActivityID"] as! String)
      batch.setData(eachActivity, forDocument: activityDoc,merge: true)
    }
    batch.commit() { err in
      if let err = err {
        print("Error writing activities batch \(err)")
      } else {
        print("Batch update of \(activitiesReadyForUpdate.count) activities succeeded.")
        print(activitiesReadyForUpdate)
        FirestoreManager.shared.updateAllCalenders(forActivities: activitiesReadyForUpdate)
        //        activitiesReadyForUpdate.forEach{ FirestoreManager.shared.updateCalender(forActivity: $0)}
      }
    }
  }
  
  public func updateAllCalenders(forActivities activities: [[String: Any]]) {
    var calenders: [[String: Any]] = []
    for activity in activities {
      guard let startDateEpoch = activity["StartDate"] as? Int,
            let activityID = activity["ActivityID"] as? String,
            let activityName = activity["ActivityName"] as? String else {
        continue
      }
      let activityDate: Date = Date.getDateFromActivityStartEpochDate(fromTimeIntervalSince1970: startDateEpoch)
      
      let calenderID = activityDate.getCalenderID()
      var currentCalenderDoc: [String: Any] = [:]
      
      let currentCalender = calenders.filter{ guard let calendarid =  $0["CalendarID"] as? String else {return false} ; return calendarid == calenderID}.first
      if let currentCalender = currentCalender {
        currentCalenderDoc = currentCalender
      } else {
        currentCalenderDoc = [:]
      }
      
      var activityIDs = (currentCalenderDoc["ActivityIDs"] as? [String]) ?? [String]()
      if activityIDs.contains(activityID) {
        continue
      }
      
      activityIDs.append(activityID)
      
      var count = (currentCalenderDoc["DailyActivityCount"] as? Int) ?? 0
      
      if activityName != "Daily Steps" {
        count += 1
      }
      
      var dailyDuration = currentCalenderDoc["DailyDuration"] as? Int
      dailyDuration = (dailyDuration ?? 0) + ((activity["Duration"] as? Int) ?? 0)
      
      var dailyCalories = currentCalenderDoc["DailyCalories"] as? Int
      dailyCalories = (dailyCalories ?? 0) + ((activity["CaloriesBurned"] as? Int) ?? 0)
      
      var dailyFitScore = currentCalenderDoc["DailyFitScore"] as? Int
      dailyFitScore = (dailyFitScore ?? 0) + ((activity["ActivityScore"] as? Int) ?? 0)
      
      var dailySteps = (currentCalenderDoc["DailySteps"] as? Int) ?? 0
      dailySteps = dailySteps + ((activity["StepCount"] as? Int) ?? 0)
      
      
      currentCalenderDoc["UserID"] = userID
      currentCalenderDoc["CalendarID"] = calenderID
      currentCalenderDoc["ActivityIDs"] = activityIDs
      currentCalenderDoc["DailyDuration"] = Int(dailyDuration ?? 0)
      currentCalenderDoc["DailyFitScore"] = Int(dailyFitScore ?? 0)
      currentCalenderDoc["RawDate"] = activityDate.getEpochValue()
      currentCalenderDoc["Date"] = activityDate.getDateString(inFormat: "dd-MM-YYYY")
      currentCalenderDoc["DailyActivityCount"] = count
      currentCalenderDoc["DailyCalories"] = Int(dailyCalories ?? 0)
      currentCalenderDoc["DailySteps"] = dailySteps
      
      if let updateItemIndex = calenders.firstIndex(where: { guard let calendarid =  $0["CalendarID"] as? String else {return false} ; return calendarid == calenderID}) {
        calenders.remove(at: updateItemIndex)
      }
      calenders.append(currentCalenderDoc)
    }
    let batch = self.db.batch()
    if calenders.count == 0 {
      return
    }
    for eachCalender in calenders {
      let calenderDocument = self.calendarRef.document(eachCalender["CalendarID"] as! String)
      batch.setData(eachCalender, forDocument: calenderDocument,merge: true)
    }
    batch.commit() { err in
      if let err = err {
        print("Error writing batch \(err)")
      } else {
        FirestoreManager.shared.updateUserData(forActivities: activities)
        print("Batch update of \(calenders.count) calenders succeeded.")
        print(calenders)
      }
    }
  }
  
  public func updateCalender(forActivity activity: [String: Any]) {
    guard let userID = userID  else { return }
    guard let startDateEpoch = activity["StartDate"] as? Int,
          let activityID = activity["ActivityID"] as? String,
          let activityName = activity["ActivityName"] as? String else {
      return
    }
    let activityDate: Date = Date.getDateFromActivityStartEpochDate(fromTimeIntervalSince1970: startDateEpoch)
    let calenderID = activityDate.getCalenderID()
    let calenderDocument = self.calendarRef.document(calenderID)
    
    
    db.runTransaction({ (transaction, errorPointer) -> Any? in
      let calDocument: DocumentSnapshot
      do {
        try calDocument = transaction.getDocument(calenderDocument)
      } catch let fetchError as NSError {
        errorPointer?.pointee = fetchError
        return nil
      }
      
      var activityIDs = (calDocument.get("ActivityIDs") as? [String]) ?? [String]()
      if activityIDs.contains(activityID) {
        errorPointer?.pointee = nil
        return nil
      }
      activityIDs.append(activityID)
      
      var count = (calDocument.get("DailyActivityCount") as? Int) ?? 0
      
      if activityName != "Daily Steps" {
        count += 1
      }
      
      var dailyDuration = calDocument.get("Duration") as? Int
      dailyDuration = (dailyDuration ?? 0) + ((activity["Duration"] as? Int) ?? 0)
      
      var dailyCalories = calDocument.get("DailyCalories") as? Double
      dailyCalories = (dailyCalories ?? 0) + ((activity["CaloriesBurned"] as? Double) ?? 0)
      
      var dailyFitScore = calDocument.get("DailyFitScore") as? Int
      dailyFitScore = (dailyFitScore ?? 0) + ((activity["ActivityScore"] as? Int) ?? 0)
      
      var dailySteps = (calDocument.get("DailySteps") as? Int) ?? 0
      dailySteps = dailySteps + ((activity["StepCount"] as? Int) ?? 0)
      
      
      let updatedCalenderDoc: [String: Any] = [
        "UserID": userID,
        "CalendarID": calenderID,
        "ActivityIDs": activityIDs,
        "DailyDuration": Int(dailyDuration ?? 0),
        "DailyFitScore": Int(dailyFitScore ?? 0),
        "RawDate": activityDate.getDayStartDate().getEpochValue(),
        "Date": activityDate.getDateString(inFormat: "dd-MM-YYYY"),
        "DailyActivityCount": count,
        "DailyCalories": Int(dailyCalories ?? 0),
        "DailySteps": dailySteps
      ]
      
      calenderDocument.setData(updatedCalenderDoc,
                               merge: true,
                               completion: { error in
                                if (error == nil ) {
                                  print("Successfully updated calender data for activities firestore")
                                  FirestoreManager.shared.updateUserData(forActivities: [activity])
                                }
                                else {
                                  print("Failed to update user activity with error - \(error!.localizedDescription)")
                                }
                               })
      return nil
    }) { (object, error) in
      if let error = error {
        print("Transaction failed: \(error)")
      } else {
        print("Transaction successfully committed!")
      }
    }
  }
  
  //
  //  func updateCalenderbyListeningToActivities() {
  //    guard let userID = userID else {
  //      return
  //    }
  //    db.collection("activities").whereField("UserID", isEqualTo: userID)
  //      .addSnapshotListener { querySnapshot, error in
  //        if error != nil {
  //          print("Error fetching documents: \(error!)")
  //        }
  //        guard let documents = querySnapshot?.documents, documents.count > 0 else {
  //          return
  //        }
  //        for eachActivity in documents {
  //          let activity = eachActivity.data()
  //          self.updateCalender(forActivity: activity)
  //        }
  //      }
  //  }
}

