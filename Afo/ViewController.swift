//
//  ViewController.swift
//  Afo
//
//  Created by mohan.k on 29/01/21.
//

import UIKit
import WebKit
import Combine
import FirebaseAuth
import SafariServices

class ViewController: UIViewController {
  static var params: [URLQueryItem] = [URLQueryItem(name: "device_type", value: "ios")]
  let testwebViewURL = "https://app.afo.fit/version-test/fitbit_testing/"
  var webViewURL: String? {
      return remoteConfig?.configValue(forKey: "afoWebURL").stringValue
//    return testwebViewURL
  }
  
  var dynamicLinkobserver: NSKeyValueObservation?
  
  @IBOutlet weak var retryButton: UIButton!
  @IBOutlet weak var noInternetView: UIView!
  var userId = ""

  
  private var userContentController = WKUserContentController()
  private var isLoading: Bool = false
  @IBAction func retryButtonTapped(_ sender: Any) {
    perfomFirstLaunchActions()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    perfomFirstLaunchActions()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    Utils.setTourAsSeen()
    if isLoading {
      self.showLoader()
    }
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  static func loadWebView() {
    let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController
    
    let homeViewController: ViewController? = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as? ViewController
    homeViewController?.loadViewIfNeeded()
    if let homevc = homeViewController{
      nav?.viewControllers = [homevc]
    }
    
    if let vcToShow = nav {
      DispatchQueue.main.async {
        UIApplication.shared.windows.first?.rootViewController = vcToShow
        UIApplication.shared.windows.first?.makeKeyAndVisible()
      }
    }
  }
  
  func perfomFirstLaunchActions() {
    dynamicLinkobserver?.invalidate()
    dynamicLinkobserver = UserDefaults.standard.observe(\.dynamicLink, options: [.initial, .new], changeHandler: { (defaults, change) in
      if let url = change.newValue, url != nil {
        let dynamicLinkFinalUrl = url! + "&device_type=ios"
        self.loadWebView(withURLString: dynamicLinkFinalUrl)
        Utils.saveFIRDynamicLinkIfAvailable(dynamicLink: nil)
      }
    })  
    retryButton.roundEdges()
    loadWebView(withURLString: webViewURL)
    Auth.auth().signInAnonymously()
    view.backgroundColor = UIColor.init(named: "primaryBackgroundColor")
    HealthKitManager.shared.syncHealthKitDataToFirestoreIfPossible()
  }
  
  private func loadWebView(withURLString urlString: String?) {
    guard let urlString = urlString, let url = URL(string: urlString) else {
      return
    }
    var urlComps = URLComponents(string: urlString)!
    urlComps.queryItems = ViewController.params
    let urlRequest = URLRequest(url: urlComps.url!)

    // To avoid zoom in webView
    let javascript = "var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
    let userScript : WKUserScript = WKUserScript(source: javascript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    
    let webConfiguration = WKWebViewConfiguration()
    webConfiguration.applicationNameForUserAgent = "Version/8.0.2 Safari/600.2.5"
    userContentController.removeScriptMessageHandler(forName: "updatePrimaryHealthSource")
    userContentController.removeScriptMessageHandler(forName: "afoBridge")

    userContentController.add(self, name: "updatePrimaryHealthSource")
    userContentController.add(self, name: "afoBridge")
    webConfiguration.userContentController = self.userContentController
    let wkwebView = WKWebView(frame: view.bounds , configuration: webConfiguration)
    
    wkwebView.configuration.userContentController.addUserScript(userScript)
    wkwebView.scrollView.minimumZoomScale = 1.0
    wkwebView.scrollView.maximumZoomScale = 1.0
    wkwebView.scrollView.bouncesZoom = false
    wkwebView.isOpaque = false
    wkwebView.navigationDelegate = self
    wkwebView.uiDelegate = self
    wkwebView.scrollView.delegate = self
    wkwebView.load(urlRequest)
    wkwebView.translatesAutoresizingMaskIntoConstraints = false
    wkwebView.scrollView.contentInsetAdjustmentBehavior = .always

    view.addSubview(wkwebView)
    view.bringSubviewToFront(wkwebView)
    view.layoutIfNeeded()
  }
  

}



extension ViewController: WKUIDelegate {
  
  func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
    guard   navigationAction.targetFrame == nil,
            let destinationUrl =  navigationAction.request.url else { return nil }
    let safariVC = SFSafariViewController(url: destinationUrl)
    self.present(safariVC, animated: true, completion: nil)
    UIApplication.shared.open(destinationUrl, options: [:],
                              completionHandler: {
                                (success) in
                                print("Open: \(success)")
                              })
    return nil
  }
  
}

extension ViewController: WKNavigationDelegate {
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    DispatchQueue.main.async {
      if webView.url!.absoluteString.contains("https://app.afo.fit") {
        self.showLoader()
        self.isLoading = true
        self.noInternetView.isHidden = true
      }
    }
    print(" webview didStartProvisionalNavigation with URL:\(String(describing: webView.url!.absoluteString))")
  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    guard let destinationUrl = navigationAction.request.url else {
      decisionHandler(.cancel);
      return
    }
    print("webview decidePolicyFor with URL:\(String(describing: destinationUrl.absoluteString))")
//    if destinationUrl.absoluteString.contains("apple_health_connect") {
//      if let params = destinationUrl.parameters {
//        if let userID = params["user_id"],
//           var dobString = params["dob"] {
//          dobString?.removeLast(3)
//          let dob = Date.decryptDate(fromDateString: dobString, usingFormat: "MMM dd, yyyy HH:mm")
//          UserDefaults.standard.setValue(dob?.yearsTillNow() ?? "", forKey: "age")
//          UserDefaults.standard.setValue(dob?.timeIntervalSince1970 ?? "", forKey: "dob")
//          UserDefaults.standard.setValue(userID, forKey: "user_id")
//          FirestoreManager.shared.setupUserData()
//          UserDefaults.standard.setValue(true, forKey: "isHealthkitSyncEnabled")
//          HealthKitManager.shared.syncHealthKitDataToFirestore()
//          decisionHandler(.cancel);
//          loadWebView(withURLString: webViewURL)
//          return
//        }
//      }
//    }
//    else
    if destinationUrl.absoluteString.contains("www.afo.fit") {
      let safariVC = SFSafariViewController(url: destinationUrl)
      self.present(safariVC, animated: true, completion: nil)
      decisionHandler(.cancel);
      return
    } else if destinationUrl.absoluteString.contains("fitbit.com") {

      let safariVC = SFSafariViewController(url: destinationUrl)
      self.present(safariVC, animated: true, completion: nil)
    }
    
    decisionHandler(.allow)
    return
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    DispatchQueue.main.async {
      self.hideLoader()
      self.isLoading = false

    }
    if let webviewURL = webView.url , webviewURL.absoluteString.contains("garmin") {
      webView.scrollView.backgroundColor = .white
      webView.backgroundColor = .white
    } else {
      webView.scrollView.backgroundColor = UIColor(named: "primaryBackgroundColor")
      webView.backgroundColor = UIColor(named: "primaryBackgroundColor")
    }
    print("webView didFinish Navigation")
  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
    print(" webview decidePolicyFor navigationResponse URL:\(String(describing: webView.url!.absoluteString))")
    decisionHandler(.allow);
  }
  
  func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
    print(" webview didReceiveServerRedirectForProvisionalNavigation with URL:\(String(describing: webView.url!.absoluteString))")
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    DispatchQueue.main.async {
      self.hideLoader()
      self.isLoading = false
      self.noInternetView.isHidden = false
    }
    print(" webview didFail with URL:\(String(describing: webView.url!.absoluteString))")
  }
  
  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    DispatchQueue.main.async {
      self.hideLoader()
      self.isLoading = false
      self.noInternetView.isHidden = false
      self.view.bringSubviewToFront(self.noInternetView)
    }
    print("webview didFailProvisionalNavigation")
  }
  
}


extension ViewController: UIScrollViewDelegate {
  public func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
    scrollView.pinchGestureRecognizer?.isEnabled = false
  }
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return nil
  }
}

extension ViewController: WKScriptMessageHandler {
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    guard let messageDict = message.body as? [String: Any] else {
      return
    }
    print(messageDict)
    

    if let primaryHealthSource = messageDict["primaryHealthSource"] as? String {
      Utils.setPrimaryHealthSource(primaryHealthSource: primaryHealthSource)
      if primaryHealthSource == "AppleHealth" {
        if let userID = messageDict["userID"] as? String {
          UserDefaults.standard.setValue(userID, forKey: "user_id")
        }
        if var  dobString = messageDict["dob"] as? String {
            dobString.removeLast(3)
            let dob = Date.decryptDate(fromDateString: dobString, usingFormat: "MMM dd, yyyy HH:mm")
            UserDefaults.standard.setValue(dob?.yearsTillNow() ?? "", forKey: "age")
            UserDefaults.standard.setValue(dob?.timeIntervalSince1970 ?? "", forKey: "dob")
          }
        FirestoreManager.shared.setupUserData()
        HealthKitManager.shared.syncHealthKitDataToFirestore()
        UserDefaults.standard.setValue(true, forKey: "isHealthkitSyncEnabled")
      } else {
        HealthKitManager.shared.stopHealthKitSync()
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.setValue(false, forKey: "isHealthkitSyncEnabled")
      }
    }

    if let accountStatus = messageDict["accountStatus"] as? String {
      if accountStatus == "Deleted" {
        HealthKitManager.shared.stopHealthKitSync()
        UserDefaults.standard.removeObject(forKey: "user_id")
      }
    }
    if let refferalLink = messageDict["referralLink"] as? String {
      self.openActivityViewController(string: refferalLink, url: nil, image: nil)
    }
//    if let accountStatus = messageDict["accountStatus"] as? String {
//      if accountStatus == "Deleted" {
//        HealthKitManager.shared.stopHealthKitSync()
//        UserDefaults.standard.removeObject(forKey: "user_id")
//      }
//    }
//    

  }
}


