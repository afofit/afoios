import HealthKit

extension HKWorkoutActivityType {

    /*
     Simple mapping of available workout types to a human readable name.
     */
    var name: String {
			switch self {
				
				case .americanFootball:             return "Football"
				case .archery:                      return "Archery"
				case .australianFootball:           return "Football"
				case .badminton:                    return "Badminton"
				case .baseball:                     return "Baseball"
				case .basketball:                   return "Basketball"
				case .boxing:                       return "Boxing"
				case .climbing:                     return "Rock climbing"
				case .cricket:												return "Cricket"
				case .crossTraining:                return "Crossfit"
				case .cycling:                      return "Cycling Outdoors"
				case .dance:                        return "Dancing"
				case .danceInspiredTraining:        return "Dancing"
				case .elliptical:                   return "Elliptical"
				case .equestrianSports:             return "Horseback riding"
				case .functionalStrengthTraining:   return "Strength Training"
				case .golf:                         return "Golf"
				case .gymnastics:                   return "Gymnastics"
				case .handball:                     return "Handball"
				case .hiking:                       return "Hiking"
				case .hockey:                       return "Hockey"
				case .lacrosse:                     return "Hockey"
				case .martialArts:                  return "Martial arts"
				case .mindAndBody:                  return "Meditation"
				case .mixedMetabolicCardioTraining: return "Other Activities High Intensity"
				case .paddleSports:                 return "Rowing on River"
				case .racquetball:                  return "Racquetball"
				case .rowing:                       return "Rowing on River"
				case .rugby:                        return "Rugby"
				case .running:                      return "Running"
				case .skatingSports:                return "Skating"
				case .soccer:                       return "Football"
				case .softball:                     return "Softball"
				case .squash:                       return "Squash"
				case .stairClimbing:                return "Stair climbing"
				case .surfingSports:                return "Surfing"
				case .swimming:                     return "Swimming"
				case .tableTennis:                  return "Table tennis"
				case .tennis:                       return "Tennis"
				case .trackAndField:                return "Running"
				case .traditionalStrengthTraining:  return "Strength Training"
				case .volleyball:                   return "Volleyball"
				case .walking:                      return "Walking"
				case .waterPolo:                    return "Waterpolo"
				case .yoga:                         return "Yoga"
					
				// iOS 10
				case .barre:                        return "Dancing"
				case .coreTraining:                 return "Core Training"
				case .crossCountrySkiing:           return "Skiing"
				case .downhillSkiing:               return "Skiing"
				case .flexibility:                  return "Crossfit"
				case .highIntensityIntervalTraining:    return "Interval Training"
				case .jumpRope:                     return "Jumping Rope"
				case .kickboxing:                   return "Kickboxing"
				case .pilates:                      return "Pilates"
				case .snowboarding:                 return "Skiing"
				case .stairs:                       return "Other Activities"
				case .stepTraining:                 return "Aerobics"
				case .wheelchairWalkPace:           return "Other Activities"
				case .wheelchairRunPace:            return "Other Activities High Intensity"
					
				// iOS 11
				case .taiChi:                       return "Tai Chi"
				case .mixedCardio:                  return "Other Activities High Intensity"
				case .handCycling:                  return "Cycling Outdoors"
					
				// iOS 13
				case .discSports:                   return "Frisbee"
				case .fitnessGaming:                return "Other Activities"
					
				case .cooldown: 									return "Yoga"
				case .socialDance:
					return "Zumba"
				// Catch-all
				default:                            return "Workout"
			}
    }

}
