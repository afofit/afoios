//
//  MasterActivity.swift
//  Afo
//
//  Created by mohan.k on 03/03/21.
//


import Foundation
// MARK: - MasterActivityData
class MasterActivityData: Codable {
    let activities: [String: Activity]?

    enum CodingKeys: String, CodingKey {
        case activities
    }

    init(activities: [String: Activity]?) {
        self.activities = activities
    }
}

// MARK: MasterActivityData convenience initializers and mutators

extension MasterActivityData {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(MasterActivityData.self, from: data)
        self.init(activities: me.activities)
    }

  convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        activities: [String: Activity]?? = nil
    ) -> MasterActivityData {
        return MasterActivityData(
            activities: activities ?? self.activities
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Activity
class Activity: Codable {
    let masterActivityID: Int?
    let meTsValue: Double?
    let category: String?
    let type: TypeEnum?

    enum CodingKeys: String, CodingKey {
        case masterActivityID
        case meTsValue = "METsValue"
        case category, type
    }

    init(masterActivityID: Int?, meTsValue: Double?, category: String?, type: TypeEnum?) {
        self.masterActivityID = masterActivityID
        self.meTsValue = meTsValue
        self.category = category
        self.type = type
    }
}

// MARK: Activity convenience initializers and mutators

extension Activity {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Activity.self, from: data)
        self.init(masterActivityID: me.masterActivityID, meTsValue: me.meTsValue, category: me.category, type: me.type)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        masterActivityID: Int?? = nil,
        meTsValue: Double?? = nil,
        category: String?? = nil,
        type: TypeEnum?? = nil
    ) -> Activity {
        return Activity(
            masterActivityID: masterActivityID ?? self.masterActivityID,
            meTsValue: meTsValue ?? self.meTsValue,
            category: category ?? self.category,
            type: type ?? self.type
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

enum TypeEnum: String, Codable {
    case distance = "distance"
    case generic = "generic"
    case speed = "speed"
    case strengthConditioning = "Strength & Conditioning"
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
