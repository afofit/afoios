//
//  HealthKitManager.swift
//  Fitty
//
//  Created by mohan.k on 28/01/21.
//

import Foundation
import HealthKit
import UIKit

class HealthKitManager {
  static var shared = HealthKitManager()
  var healthStore: HKHealthStore?
  var masterActivityData : MasterActivityData?
  var heartRateStatisticsCollection: HKStatisticsCollection?
  var isSyncing = false
  var maxHeartRate: Double = 0
  let readTypes = Set([HKObjectType.workoutType(),
                       HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!,
                       HKObjectType.quantityType(forIdentifier: .stepCount)!,
                       HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!,
                       HKObjectType.quantityType(forIdentifier: .heartRate)!,
                       HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
                       HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.mindfulSession)!,
                       HKSeriesType.workoutRoute()])
  var allActivities: [[String: Any]] = []
  var dispatchGroup: DispatchGroup?
  var fiftyPerCentHeartRate: Double {
    get {
      return maxHeartRate/2
    }
  }
  
  var seventyPerCentHeartRate: Double {
    get {
      return maxHeartRate * 0.7
    }
  }
  
  private var runningHealthKitQueries = [HKQuery]()
  var primaryHealthSourceObserver: NSKeyValueObservation?
  var accountStatusObserver: NSKeyValueObservation?
  private init() {
    if HKHealthStore.isHealthDataAvailable() {
      self.healthStore = HKHealthStore()
    } else {
      print("ERROR: HealthKit is not supported in this device")
    }
    
    if let masterActivityDataString = remoteConfig?.configValue(forKey: "masterActivityData").stringValue {
      masterActivityData = try! MasterActivityData(masterActivityDataString);
    }
    
    let age = (UserDefaults.standard.value(forKey: "age") as? Double) ?? 0.0
    self.maxHeartRate = 205.8 - (0.68 * age)
    
    //    primaryHealthSourceObserver = UserDefaults.standard.observe(\.primaryHealthSource, options: [.initial, .new], changeHandler: { (defaults, change) in
    //      if let url = change.newValue, url == "AppleHealth" {
    //        self.syncHealthKitDataToFirestore()
    //      } else {
    //        self.stopHealthKitSync()
    //      }
    //    })
//    accountStatusObserver = UserDefaults.standard.observe(\.primaryHealthSource, options: [.initial, .new], changeHandler: { (defaults, change) in
//      if let url = change.newValue, url == "AppleHealth" {
//        self.syncHealthKitDataToFirestore()
//      } else if change.newValue == nil {
//        self.stopHealthKitSync()
//      }
//    })
  }
  
  public func stopHealthKitSync() {
    self.runningHealthKitQueries.forEach{self.healthStore?.stop($0)}
    self.runningHealthKitQueries.removeAll()
    self.isSyncing = false
    UserDefaults.standard.setValue(false, forKey: "isHealthkitSyncEnabled")
  }
  
  public func syncHealthKitDataToFirestore(showPopup: Bool = true) {
    self.runningHealthKitQueries.forEach{self.healthStore?.stop($0)}
    self.runningHealthKitQueries.removeAll()
    UserDefaults.standard.setValue(true, forKey: "isHealthkitSyncEnabled")
    if UserDefaults.standard.bool(forKey: "haveRequestedHealthDataPermissions") {
      self.showAlertAndRedirectToHealthPermission()
      self.startHealthKitSync()
    } else if showPopup {
      self.showHealthKitPermissionPopup()
    }
  }
  
  public func syncHealthKitDataToFirestoreIfPossible() {
    if UserDefaults.standard.bool(forKey: "haveRequestedHealthDataPermissions") && UserDefaults.standard.bool(forKey: "isHealthkitSyncEnabled") {
      self.startHealthKitSync()
    }
  }
  
  func showHealthKitPermissionPopup() {
    let okAction: ((UIAlertAction) -> Void) = { [weak self] _ in
      self?.healthStore?.requestAuthorization(toShare: Set(), read: self?.readTypes) { (success, error) in
        if error != nil {
          UserDefaults.standard.setValue(false, forKey: "haveRequestedHealthDataPermissions")
          return
        }
        self?.startHealthKitSync()
        UserDefaults.standard.setValue(true, forKey: "haveRequestedHealthDataPermissions")
      }
    }
    Utils.showHealthAlert(title: "HealthKit Permission",
                          withMessage: "Please provide all permissions in Health App if not given.",
                          okCompletionHandler: okAction)
  }
  
  
  func showAlertAndRedirectToHealthPermission(withTitle title: String = "HealthKit Permission",
                                              withMessage message: String = "You will be redirected to Health app. Please go to profile > Apps > afo") {
    let okAction: ((UIAlertAction) -> Void) = { _ in
      openUrl(urlString: "x-apple-health://")
    }
    let cancelAction: ((UIAlertAction) -> Void) = {_ in
      self.startHealthKitSync()
    }
    Utils.showHealthAlert(title: title,
                          withMessage: message,
                          okCompletionHandler: okAction,
                          cancelCompletionHandler: cancelAction)

  }
  
  private func checkAuthotizationStatusForHealthKit(forHKObject hkObject: HKObjectType) -> Bool {
    let hkAuthStatus: HKAuthorizationStatus = (self.healthStore?.authorizationStatus(for: hkObject))!
    if hkAuthStatus == .notDetermined || hkAuthStatus == .sharingDenied {
      print("ERROR: HealthKit sharing not authorized")
      return false
    }
    return true
  }
  
  private func startHealthKitSync() {
    if !isSyncing {
      self.isSyncing = true
      print("HealthKit started syncing data to firebase")
      FirestoreManager.shared.setupUserData {
        if self.heartRateStatisticsCollection == nil {
          var interval = DateComponents()
          interval.minute = 1
          self.runHeartRateStatisticsCollectionQuery(interval: interval) {success, statsCollection in
            self.heartRateStatisticsCollection = statsCollection
            
            self.dispatchGroup = DispatchGroup()
            self.enableBackgroundDelivery()
            self.dispatchGroup?.enter()
            self.runWorkoutsAnchoredObjectQuery()
            
            self.dispatchGroup?.enter()
            self.runDailyStepsAnchoredObjectQuery()
            
            self.dispatchGroup?.notify(queue: .global(qos: .background), execute: {
              print("All queries done, uploading \(self.allActivities.count) activities to firebase")
              FirestoreManager.shared.updateActivities(activities: self.allActivities)
              self.dispatchGroup = nil
              self.allActivities.removeAll()
            })
            
          }
          
        }
      }
    } else {
      print("HealthKit already syncing data to firebase")
    }
    
  }
  
  //MARK: Fixme
  private func calculateActivityScore(forActivity workoutActivity: HKWorkout?,
                                      withCompletion completion: @escaping (_ activity:[String: Any]?) -> Void) {
    guard let workoutActivity = workoutActivity else { return }
    var activityScoreForCurrentActivity = 0.0
    if let heartRateStatisticsCollection = self.heartRateStatisticsCollection {
      heartRateStatisticsCollection.enumerateStatistics(from: workoutActivity.startDate.roundedToClosestMinute(),
                                                        to: workoutActivity.endDate.roundedToClosestMinute())
      { statistics, stop in
        if let quantity = statistics.averageQuantity() {
          let value = quantity.doubleValue(for: HKUnit(from: "count/min"))
          if value < self.seventyPerCentHeartRate && value > self.fiftyPerCentHeartRate {
            activityScoreForCurrentActivity += 1
          } else if value >= self.seventyPerCentHeartRate {
            activityScoreForCurrentActivity += 2
          } else {
            activityScoreForCurrentActivity += 0
          }
        }
      }
      if activityScoreForCurrentActivity <= (workoutActivity.duration/60) {
        activityScoreForCurrentActivity = self.calculateActivityScoreUsingMetValues(forActivity: workoutActivity)
        activityScoreForCurrentActivity.round()
        completion(self.updateActivityScoreInActivity(workoutActivity: workoutActivity,
                                                      activityScore: Int(activityScoreForCurrentActivity)))
      }
      else {
        activityScoreForCurrentActivity.round()
        completion(self.updateActivityScoreInActivity(workoutActivity: workoutActivity,
                                                      activityScore: Int(activityScoreForCurrentActivity),
                                                      scoredUsingHeartRate: true))
      }
      
    }
    
    else {
      activityScoreForCurrentActivity = self.calculateActivityScoreUsingMetValues(forActivity: workoutActivity)
      activityScoreForCurrentActivity.round()
      completion(self.updateActivityScoreInActivity(workoutActivity: workoutActivity,
                                                      activityScore: Int(activityScoreForCurrentActivity)))
    }
    
  }
  
  private func calculateActivityScoreUsingMetValues(forActivity workoutActivity: HKWorkout) -> Double {
    var currentActivityData: Activity?
    for (value) in masterActivityData!.activities!.enumerated() {
      let activityName = value.element.key
      if activityName.contains(workoutActivity.workoutActivityType.name) {
        currentActivityData = value.element.value
        break
      }
    }
    let duration = workoutActivity.duration
    let currentActivityMetValue = currentActivityData?.meTsValue ?? 0
    if 3 < currentActivityMetValue && currentActivityMetValue < 6 {
      return duration/60 * 1
    } else if currentActivityMetValue >= 6{
      return duration/60 * 2
    }
    return 0
  }
  
  func updateActivityScoreInActivity(workoutActivity: HKWorkout, activityScore activityScoreForCurrentActivity: Int, scoredUsingHeartRate: Bool = false) -> [String: Any]? {
    
    let distanceInMeters = Int(workoutActivity.totalDistance?.doubleValue(for:  HKUnit.meter()) ?? 0)
    let durationInSeconds = Int(workoutActivity.startDate.distanceTo(to: workoutActivity.endDate))
    let durationInMin = Double(durationInSeconds)/60.0
    var averagePaceInKMPH = 0.0
    if durationInSeconds > 0 {
      averagePaceInKMPH = Double((distanceInMeters / durationInSeconds)) * 0.06
    }
    
    var userActivity: [String : Any] = [
      "UserID": (userID) ?? "NA",
      "ActivityID": (userID ?? "") + "_" + Date.getFormattedString(fromDate: workoutActivity.startDate),
      "ActivityName": workoutActivity.workoutActivityType.name ,
      "StartDate": workoutActivity.startDate.getEpochValue(),
      "EndDate": workoutActivity.endDate.getEpochValue(),
      "Duration": Int(durationInMin.rounded()),
      "Source": "AppleHealth",
      "CaloriesBurned": Int(workoutActivity.totalEnergyBurned?.doubleValue(for: HKUnit.largeCalorie()) ?? 0),
      "Distance": distanceInMeters,
      "ActivityScore": Int(activityScoreForCurrentActivity),
      "StepCount": 0,
      "AveragePace": averagePaceInKMPH.rounded(),
      "sourcedFrom": "AppleHealth",
      "scoredUsingHeartRate": scoredUsingHeartRate ? 1 : 0
    ]
    
    if workoutActivity.workoutActivityType == .running || workoutActivity.workoutActivityType == .walking {
      if distanceInMeters < 500 {return nil}
      var averagePaceMinPerKm = 0.0
      if distanceInMeters > 0 {
        averagePaceMinPerKm = (durationInMin/Double(distanceInMeters)) * 1000
      }
      var activityScore = 0
      if averagePaceMinPerKm > 8.5 && averagePaceMinPerKm < 15 {
        activityScore = Int(durationInMin.rounded())
      } else if averagePaceMinPerKm <= 8.5 {
        activityScore = Int(durationInMin.rounded() * 2)
      }
      userActivity["ActivityScore"] = Int(activityScore)
      userActivity["AveragePace"] = averagePaceMinPerKm.rounded()
      userActivity["Duration"] = Int(durationInMin.rounded())
    }
    return userActivity
  }
  
  //MARK: - HealthKit Queries
  private func runHeartRateStatisticsCollectionQuery( interval: DateComponents,
                                                      withCompletion completion: @escaping (_ success: Bool,_ statistics :HKStatisticsCollection?) -> Void) {
    
    guard let heartRateType: HKQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else {return}
    // Create the query
    let query = HKStatisticsCollectionQuery(quantityType: heartRateType,
                                            quantitySamplePredicate: nil,
                                            options: .discreteAverage,
                                            anchorDate: Date().roundedToClosestMinute(),
                                            intervalComponents: interval)
    // Set the results handler
    query.initialResultsHandler = {
      query, results, error in
      if let error = error {
        self.showAlertAndRedirectToHealthPermission(withTitle: "Heartrate statistics are not available!", withMessage: "Plese check your heart rate permission")
        completion(false, nil);
        
      }
      
      guard let statsCollection = results else {
        return
      }
      self.heartRateStatisticsCollection = statsCollection
      completion(true, statsCollection);
    }
    query.statisticsUpdateHandler = {
      query, statistics, statsCollection, error in
      if let error = error {
        self.showAlertAndRedirectToHealthPermission(withTitle: "Heartrate statistics are not available!", withMessage: "Plese check your heart rate permission")
        completion(false, nil);
        
      }
      
      guard let statsCollection = statsCollection else {
        completion(false, nil);
        return
      }
      self.heartRateStatisticsCollection  = statsCollection
      completion(true, statsCollection);
      
    }
    healthStore?.execute(query)
  }
  
  func enableBackgroundDelivery() {
    healthStore?.enableBackgroundDelivery(for: HKObjectType.workoutType(), frequency: .immediate, withCompletion: {_,_ in})
    healthStore?.enableBackgroundDelivery(for: HKObjectType.quantityType(forIdentifier: .stepCount)!, frequency: .immediate, withCompletion: {_,_ in})
  }
  
  private func runDailyStepsAnchoredObjectQuery() {
    guard let stepCountType: HKQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount) else {return}
    var interval = DateComponents()
    interval.day = 1
    
    let anchorDate = Date().getDayStartDate()
    let query = HKStatisticsCollectionQuery(quantityType: stepCountType,
                                            quantitySamplePredicate: nil,
                                            options: .cumulativeSum,
                                            anchorDate: anchorDate,
                                            intervalComponents: interval)
    
    // Set the results handler
    query.initialResultsHandler = {
      query, results, error in
      defer {
        self.dispatchGroup?.leave()
        print("Daily steps query Completed")

      }
      if (error != nil) {
        print("\(query) failed with error \(String(describing: error?.localizedDescription))")
        self.showAlertAndRedirectToHealthPermission(withTitle: "Error while updating Daily steps", withMessage: error?.localizedDescription ?? "Please provide health hpermissions to afo.")
        return
      }
      guard let statsCollection = results else {
        return
      }
      
      
      let startDate = Utils.getLastActivityUpdatedTime()
      let endDate = Date()
      
      statsCollection.enumerateStatistics(from: startDate, to: endDate) { [unowned self] statistics, stop in
        
        if let quantity = statistics.sumQuantity()
        {
          let date = statistics.startDate
          let value = quantity.doubleValue(for: HKUnit.count())
          let activityScore = 0.002 * value
          
          let dailyStepsActivity: [String : Any] = [
            "UserID": (userID) ?? "NA",
            "ActivityID": (userID ?? "") + "_" + Date.getFormattedString(fromDate: date),
            "ActivityName": "Daily Steps" ,
            "StartDate": date.getEpochValue(),
            "EndDate": date.getEpochValue(),
            "Source": "AppleHealth",
            "Distance": 0,
            "CaloriesBurned": 0,
            "Duration": 0,
            "StepCount": Int(value),
            "AveragePace": 0,
            "ActivityScore": Int(round(activityScore))
          ]
          self.allActivities.append(dailyStepsActivity)
        }
      }
    }
    
    query.statisticsUpdateHandler = {
      query,statistics, results, error in
      if (error != nil) {
        print("\(query) failed with error \(String(describing: error?.localizedDescription))")
        self.showAlertAndRedirectToHealthPermission(withTitle: "Error while updating Daily steps", withMessage: error?.localizedDescription ?? "Please provide health hpermissions to afo.")
        return
      }
      guard let statsCollection = results, let statistics = statistics else {
        // Perform proper error handling here
        return
      }
      
      let startDate = Utils.getLastActivityUpdatedTime()
      let endDate = Date()
      statsCollection.enumerateStatistics(from: startDate, to: endDate) { [unowned self] statistics, stop in
        if let quantity = statistics.sumQuantity()
        {
          let startDate = statistics.startDate
          let endDate = statistics.endDate
          let value = quantity.doubleValue(for: HKUnit.count())
          let activityScore = 0.002 * value
          
          let dailyStepsActivity: [String : Any] = [
            "UserID": (userID) ?? "NA",
            "ActivityID": (userID ?? "") + "_" + Date.getFormattedString(fromDate: startDate),
            "ActivityName": "Daily Steps" ,
            "StartDate": startDate.getEpochValue(),
            "EndDate": endDate.getEpochValue(),
            "Source": "AppleHealth",
            "Distance": 0,
            "CaloriesBurned": 0,
            "Duration": 0,
            "StepCount": Int(value),
            "AveragePace": 0,
            "ActivityScore": Int(round(activityScore))
          ]
          FirestoreManager.shared.updateUserActivity(activity: dailyStepsActivity)
        }
      }
    }
    self.runningHealthKitQueries.append(query)
    healthStore?.execute(query)
  }
  
  func updateDailyStepActivity(forStatistics statistics: HKStatistics) {
    if let quantity = statistics.sumQuantity()
    {
      let date = statistics.startDate
      let value = quantity.doubleValue(for: HKUnit.count())
      let activityScore = 0.002 * value
      
      let dailyStepsActivity: [String : Any] = [
        "UserID": (userID) ?? "NA",
        "ActivityID": (userID ?? "") + "_" + Date.getFormattedString(fromDate: date),
        "ActivityName": "Daily Steps" ,
        "StartDate": date.getEpochValue(),
        "EndDate": date.getEpochValue(),
        "Source": "AppleHealth",
        "Distance": 0,
        "CaloriesBurned": 0,
        "Duration": 0,
        "StepCount": Int(value),
        "AveragePace": 0,
        "ActivityScore": Int(round(activityScore))
      ]
    }
  }
  
  private func runWorkoutsAnchoredObjectQuery() {
    //Run AnchoredQuery for all the workoutTypes
    
    self.runHealthKitAnchoredQuery() {[weak self] success, samples in
      defer {
        DispatchQueue.main.async {
          self?.dispatchGroup?.leave()
        }
      }
      if !success {
        print("HealthKit AnchoredQuery failed")

        return
      }
      guard let samples = samples, samples.count > 0 else {
        print("No new workout samples received")
        return
      }
      print("Received \(samples.count) new workout samples")
      for sample in samples {
        if let workoutActivity = sample as? HKWorkout {
          self?.calculateActivityScore(forActivity: workoutActivity) { activity in
            if let activity = activity {
              self?.allActivities.append(activity)
              print("appending activity")
            }
          }
        } else {
          continue
        }
      }

    }
  }
  
  private func runHealthKitAnchoredQuery(withCompletion completion: @escaping (_ success:Bool, _ samples: [HKSample]?) -> Void) {
    var myAnchor: HKQueryAnchor? = nil
    var workoutPredicatesArray: [NSPredicate] = []
    for i in 1...80 {
      guard let workoutActivityType: HKWorkoutActivityType = HKWorkoutActivityType(rawValue: UInt(i)) else {
        return
      }
      let activityPredicate = HKQuery.predicateForWorkouts(with: workoutActivityType)
      workoutPredicatesArray.append(activityPredicate)
    }
    let lut = Utils.getLastActivityUpdatedTime()
    let timePredicate = HKQuery.predicateForSamples(
      withStart: lut,
      end: Date(),
      options: .strictStartDate
    )
    let compundWorkoutPredicates = NSCompoundPredicate(orPredicateWithSubpredicates: workoutPredicatesArray)
    let compTimedWorkoutsPred = NSCompoundPredicate(andPredicateWithSubpredicates: [compundWorkoutPredicates, timePredicate])
    let query = HKAnchoredObjectQuery(type: HKSampleType.workoutType(),
                                      predicate: compTimedWorkoutsPred,
                                      anchor: myAnchor,
                                      limit: HKObjectQueryNoLimit)
    { (query, samplesOrNil, deletedObjectsOrNil, newAnchor, errorOrNil) in
      
      if let error = errorOrNil {
        print("\(query) failed with error \(String(describing: error.localizedDescription))")
        self.showAlertAndRedirectToHealthPermission(withTitle: "Error while updating your workout activities", withMessage: error.localizedDescription)
        completion(false,nil)
      }
      guard let samples = samplesOrNil, let _ = deletedObjectsOrNil else {
        // Properly handle the error.
        self.showAlertAndRedirectToHealthPermission();
        completion(false,nil)
        return
      }
      
      if let newAnchor = newAnchor {
        myAnchor = newAnchor
      }
      completion(true, samples);
    }
    
    query.updateHandler = { (query, samplesOrNil, deletedObjectsOrNil, newAnchor, errorOrNil) in
      if let error = errorOrNil {
        print("\(query) failed with error \(String(describing: error.localizedDescription))")
        self.showAlertAndRedirectToHealthPermission(withTitle: "Error while updating your workout activities", withMessage: error.localizedDescription)
        completion(false,nil)
      }
      guard let samples = samplesOrNil, let deletedObjects = deletedObjectsOrNil else {
        // Properly handle the error.
        return
      }
      if let newAnchor = newAnchor {
        //        Utils.updateAnchor(forKey: anchorkey, anchor: newAnchor)
        myAnchor = newAnchor
      }
      for sample in samples {
        guard  let workoutActivity = sample as? HKWorkout else {
          return
        }
        self.calculateActivityScore(forActivity: workoutActivity) { activity in
          guard let activity = activity else {
            return
          }
          FirestoreManager.shared.updateUserActivity(activity: activity)
        }
      }
    }
    self.runningHealthKitQueries.append(query)
    self.healthStore?.execute(query)
    
  }
  
  static func saveMockHeartData(from : Date, to :Date) {
    
    // 1. Create a heart rate BPM Sample
    let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    let heartRateQuantity = HKQuantity(unit: HKUnit(from: "count/min"),
                                       doubleValue: Double(arc4random_uniform(80) + 100))
    let halfDistance = from.distanceTo(to: to)/2
    let halfTime = from.addingTimeInterval(TimeInterval(halfDistance))
    
    let heartSample1 = HKQuantitySample(type: heartRateType,
                                        quantity: heartRateQuantity, start: from, end: halfTime)
    
    let heartSample2 = HKQuantitySample(type: heartRateType,
                                        quantity: heartRateQuantity, start: halfTime, end: to)
    
    
    // 2. Save the sample in the store
    shared.healthStore?.save([heartSample1,heartSample2], withCompletion: { (success, error) -> Void in
      if let error = error {
        print("Error saving heart sample: \(error.localizedDescription)")
      }
    })
  }
  
}


