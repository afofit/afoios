//
//  LogoAnimationViewController.swift
//  Afo
//
//  Created by mohan.k on 18/03/21.
//

import UIKit
import SwiftyGif

class LogoAnimationViewController: UIViewController {
  
  var gifView: LogoAnimationView!
  var homeViewController: ViewController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    homeViewController = storyboard?.instantiateViewController(withIdentifier: "homeViewController") as? ViewController
    homeViewController?.loadViewIfNeeded()

    showGif()
  }
  
  
}
extension LogoAnimationViewController: SwiftyGifDelegate {
  private func showGif() {
    gifView = LogoAnimationView(frame: view.bounds)
    guard let gifView = gifView  else {
      print("Error: Unable to create gifView")
      return
    }
    gifView.contentMode = .scaleAspectFill
    gifView.frame = view.frame;
    gifView.logoGifImageView.delegate = self
    view.addSubview(gifView)
    view.backgroundColor = .clear
    gifView.logoGifImageView.startAnimatingGif()
  }
  
  func gifDidStop(sender: UIImageView) {
    var vc: UIViewController?
    if Utils.hasShownTour() {
      let nav = storyboard?.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController
      if let homevc = homeViewController{
        nav?.viewControllers = [homevc]
      }
      vc = nav
    } else {
      vc = storyboard?.instantiateViewController(withIdentifier: "TourViewController")
    }
    guard let vcToShow = vc else {
      return
    }
    DispatchQueue.main.async {
      UIApplication.shared.windows.first?.rootViewController = vcToShow
      UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
  }
  
}
